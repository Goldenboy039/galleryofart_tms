	// create the module and name it scotchApp
	var NGAcoopApp = angular.module('NGAcoopApp', ['ngRoute']);

	// configure our routes
	NGAcoopApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'index.html',
				controller  : 'mainController'
			})

			// route for the about page
			.when('/regularloan', {
				templateUrl : 'views/regularloan.html',
				controller  : 'regularloanController'
			})

			
	});

	// create the controller and inject Angular's $scope
	NGAcoopApp.controller('mainController', function($scope) {
		// create a message to display in our view
		$scope.message = 'Everyone come and see how good I look!';
	});

	NGAcoopApp.controller('regularloanController', function($scope) {
		$scope.message = 'Look! I am an about page.';
	});
